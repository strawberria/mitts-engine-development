## 1.5.4 (07/21/2022)
- Note: undiagnosed error egarding InteractionCriteriaCurrent
- Made selecting exceededAttempts criteria clear and disable the interaction's action and components inputs

## 1.5.3 (07/21/2022)
- Added undo feature for runtime which restores last action
- Fixed weird scrolling behavior for in-development playtester

## 1.5.2 (07/21/2022)
- Fixed one-argument actions not working properly
- Fixed examine not working properly due to debugging
- Fixed exceeded attempts not working properly

## 1.5.1 (07/21/2022)
- Added missing `minimap-display` class for in-game playtesting

## 1.5.0 (07/20/2022)
- Added in-app playtester for game with debug info accessible through the inspect element console 
- (untested) Added interaction criteria for number of attempts within current state (reset automatically whenever state changed)
- (untested) Added interaction result for resetting the number of attempts within the current state 
- Changed ending types from `goodEnd` and `badEnd` to simply `end`, opting to replace "Good End" and "Bad End" with an input labelled "End Line"
- Also made interactions and minimap tab inaccessible for opening states

## 1.4.6 (07/14/2022)
- Correctly made minimap tab inaccessible for ending state (already did for opening state)

## 1.4.5 (07/14/2022)
- Fixed a related issue caused by selecting a minimap object and changing states (nested)

## 1.4.4 (07/14/2022)
- Fixed loading images not properly allowing all image types (png, jpg, jpeg, gif, webp)
- *Properly* fixed crashing due to nested interactions within states when state is changed

## 1.4.3 (07/14/2022)
- Fixed bug `Cannot read properties of undefined (reading 'action')` within `InteractionCriteriaSelector.svelte`

## 1.4.2 (07/14/2022)
- Fixed bug `Cannot read properties of undefined (reading 'action')` within `InteractionCurrent.svelte`
- Fixed bug `Cannot read properties of undefined (reading 'devname')` in the context of Svelte component generation within `InteractionResultCurrent.svelte`